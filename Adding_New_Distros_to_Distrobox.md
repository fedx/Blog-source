---
title: Adding new Distros to Distrobox
description: How I got SteamOS in a podman container with GUIs. 
date: 2022-08-22
---
Distrobox is an amazing tool! It allows you to run countless distros inside Podman (or Docker) on your host with the important parts of the host mounted inside. This creates a rather unforseen problem with OCI images, they are all meant for servers, but, Distrobox is treating them more like a desktop. This means, most dedicated desktop distros do not have an image that can be used to create a container. There were two in particular I was interested in trying: Pop! OS as I am on a System76 laptop and Pop is very useful, and it's just plain better then Ubuntu, and SteamOS because I am curius to see what all the hype is about. Now, I could just bootstrap a distro with a container image to the one I want, but where's the fun in that! 
## Pop OS 
The simpler of the two by far was Pop! First of all, you will need to grab the ISO, from System76. 
Part of what's cool about ISO images, is that you can simply mount them with `sudo mount </path/to/iso> </path/to/mount-dir>`. 

That will give us access to the raw rootfs of the live installer for Pop! However, this is still not the rootfs of the disrto istelf. For that we need to find the `rootfs.squashfs`. A great tool fot that is `fd`, so simply run `fd squashfs` and it should look something like `casper_pop-os_22.04_amd64_intel_debug_152/filesystem.squashfs`. That is what we want to import into Docker/Podman, but they do not understand squashfs, so we must unsquash it with ` sudo unsquashfs -f -d roootfs </path/to/filesystem.squashfs>`. 

We now have a raw rootfs of Pop! However, we still cannot import that into Docker/Podman dirrectly, but because we can stack together Linux tools, we can use the command `sudo tar -C rootfs/ -c . | docker import - popos/latest` (if you are using Podman, make sure Docker compatibility is enabled). 

Now if you run `podman images` you will see something like `localhost/popos/latest                     latest                   2541c585efb1  23 hours ago   13.5 GB`. 

To create a Distrobox container using that image, simply run `distrobox crate --name popos --image localhost/popos/latest`. 

It's certainly not the most space efficant container, but it certainly works. 

## SteamOS
This one is a bit more complicated due to the fact that SteamOS 3.0 does not exactly exist yet, but that's also what makes it fun! 

Fist off, grab a [SteamDeck](https://help.steampowered.com/en/faqs/view/1B71-EDF2-EB6D-2BB3) recvoery image. This is an img, not an iso, so we can't jsut `sudo mount`. Instead, we need to create a loopback devcie, by running `sudo losetup -f -P steamdeck-recovery-4.img`.

Runnling `lsblk -f` should reveal a loop device that looks something like: 
```
loop1
├─loop1p1   vfat    FAT32            esp                      492E-154F
├─loop1p2   vfat    FAT32            efi                      4934-F915
├─loop1p3   btrfs                    rootfs                   304d3526-c99e-45e9-81b5-b8cf4367a4a7
├─loop1p4   ext4    1.0              var                      17d46966-cb62-416d-8c9d-603d5fde06c4
└─loop1p5   ext4    1.0              home                     952128a7-3815-484e-9fe9-423858e036b3
```

We are wanting to extract the btrfs rootfs, so I would run `sudo mount /dev/loop1p3 </path/to/mont-dir>`. 

SteamOS uses an read-only root with A/B partition updating. This does not work in a container, so run `sudo cp -r rootfs/* new-rootfs`, because the SteamOS image does not contain everything for Pacman to work out of the box. To ensure Pacman works, run `mkdir -p new-rootfs/var/lib/pacman/local`. Now import it into Podman like before by running `sudo tar -C new-rootfs/ -c . | docker import - steamos/latest`. 

We now have a SteamOS image, but it still will not work. Why? Due to the A/B rootfs, Pacman is a bit borked in the image. So, I use the following Containerfile to force Pacman to work propperly: 

```
FROM localhost/steamos/latest
RUN pacman -Syy --noconfirm 
run pacman-db-upgrade
RUN pacman -Syuu --noconfirm
RUN pacman -S --noconfirm --overwrite "*" bc curl coreutils findutils less ncurses  pinentry sudo util-linux wget vte-common bash xorg
RUN chown -R root /etc
RUN userdel deck
```
After running `podman build .` I can finaly create a distrobox by running `distrbox create --name steamos --image <image-hash>` and I have a working SteamOS distrobox. 

### Known Issues: 
This containerfile is still a work in progress, and I will probably update it in the future. That said, the big issue I have noticed is Pacman givivin errors along the lines of `xorg-server: /usr/share/licenses/xorg-server/COPYING exists in filesystem`. If this happends, just run `sudo pamcan -S --noconfirm --overwrite "*" <package-name>`. I am changing up the containerfile to mitigate this in the future, but I have not  done so yet. 



